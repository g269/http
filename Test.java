package com.http.test;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author 高健，后端笔试
 *
 *第二题：开发题
不依赖任何第三方库或框架(只使用jdk里面包含的公开可用的类库),tomcat属于第三方框架所以不要用
com.sun下的也不要用，开发一个http服务 做加法运算: 计算参数a和参数b的算术和 -- 
在浏览器地址栏里输入`http://localhost/add?a=4&b=7`，浏览器窗口中显示11。
这个http服务同时还要 能做乘法运算 计算参数a和参数b的算术乘积 -- 
在浏览器地址栏里输入`http://localhost/mult?a=4&b=7`，浏览器窗口中显示28。
把代码放到gitlab.com上。
然后把项目的url写在答案文件中。
 */

public class Test {

    //服务器返回数据必要的数据头
    private static String head = "HTTP/1.1 200 OK"
                                +"Connection: close"
                                +"\n\n";
    public static void main(String[] args) throws IOException{
        System.out.println("服务器启动");
        //  端口:
        ServerSocket socket = new ServerSocket(80);
        while(true){
        //  等待浏览器请求
            Socket brower = socket.accept();
            //  一次性读取浏览器请求发来的请求头,避免阻塞
            InputStream receiver = brower.getInputStream();
            byte[] buf = new byte[1024];
            int len = receiver.read(buf);
            String str=new String(buf,0,len);
            String parme=str.substring(str.indexOf("/"),str.indexOf("HTTP/1.1")-1);///add?a=4&b=7
            if(parme.indexOf("?")>0){
            	String type=parme.substring(1,parme.indexOf("?"));
                String a=parme.substring(parme.indexOf("=")+1, parme.indexOf("&"));
                String b=parme.substring(parme.lastIndexOf("=")+1);
                
                
                System.out.println("parme=="+parme+",a=="+a+",b=="+b);
               
                OutputStream sender = brower.getOutputStream();
               
                String res="Request Success";
                if("add".equals(type)){
                	res=Integer.parseInt(a)+Integer.parseInt(b)+"";
                }else if("mult".equals(type)){
                	res=Integer.parseInt(a)*Integer.parseInt(b)+"";
                }
                String html = "<font color = 'red' size='80'>"+res+"</font>";
                String msg = head+html;
                sender.write(msg.getBytes());
                //  服务关闭
                sender.close();
                receiver.close();
                brower.close();
            }
        }
        
       // socket.close();
    }
}
